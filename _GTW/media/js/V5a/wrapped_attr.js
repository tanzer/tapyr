// Copyright (C) 2016-2024 Christian Tanzer All rights reserved
// tanzer@gg32.com                                      https://www.gg32.com
// #*** <License> ************************************************************#
// This module is licensed under the terms of the BSD 3-Clause License
// <https://www.gg32.com/license/bsd_3c.html>.
// #*** </License> ***********************************************************#
//
//++
// Name
//    V5a/wrapped_attr.js
//
// Purpose
//    Change attributes of all elements of wrapped set
//
// Revision Dates
//    25-Jan-2016 (CT) Creation
//    14-May-2024 (CT) Divide into `get_attr`, `remove_attr`, `set_attr`
//    ««revision-date»»···
//--

;
( function ($) {
    "use strict";

    $.$$.prototype.get_attr = function get_attr (name) {
        var result  = [];
        var _get    = function _get (el) {
            var val = el.getAttribute (name);
            if (val !== null) {
                result.push (val);
            };
        };
        this.for_each (_get);
        return result;
    };

    $.$$.prototype.remove_attr = function remove_attr (name) {
        this.for_each (function (el) { el.removeAttribute (name); });
    };

    $.$$.prototype.set_attr = function set_attr (name, val) {
        if (val === null) {
            this.remove_attr (name); ;
        } else {
            this.for_each (function (el) { el.setAttribute (name, val); });
        };
    };
  } ($V5a)
);

// __END__ V5a/wrapped_attr.js
