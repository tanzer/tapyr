# -*- coding: utf-8 -*-
# Copyright (C) 2014-2024 Christian Tanzer All rights reserved
# tanzer@gg32.com                                      https://www.gg32.com
# #*** <License> ************************************************************#
# This module is part of the package CHJ.CSS.
#
# This module is licensed under the terms of the BSD 3-Clause License
# <https://www.gg32.com/license/bsd_3c.html>.
# #*** </License> ***********************************************************#
#
#++
# Name
#    CHJ.CSS.Property
#
# Purpose
#    CSS property specification
#
# Revision Dates
#    12-Apr-2014 (CT) Creation
#    13-Apr-2014 (CT) Add `Value`, `Align_Items`, `Align_Self`, `Display`,
#                     `Justify_Content`, `Order`, `Width`; factor `_Prop_`
#     9-Jul-2014 (CT) Add `* args` to `Property.__call__`
#    11-Oct-2016 (CT) Move from `GTW` to `CHJ`
#    27-Dec-2016 (CT) Add `Flex_Direction`, `Scroll_Snap_Align`,
#                     `Scroll_Snap_Type`
#    27-Dec-2016 (CT) Remove `-webkit` prefix from flex properties/values
#    27-Dec-2016 (CT) Remove `-moz` prefix from `Box`, `Font_Feat`
#    11-Jan-2017 (CT) Add `Align_Content`
#    12-Jan-2017 (CT) Support symbolic arguments in `Property.__call__`
#                     * add `as_text`
#    12-Jan-2017 (CT) Remove `Calc`
#    18-Jan-2017 (CT) Remove `-webkit` prefix from `Box`
#    19-Aug-2019 (CT) Use `print_prepr`
#     5-May-2024 (CT) Remove vendor prefixes
#    ««revision-date»»···
#--

"""
Specification of CSS properties::

    >>> from   _TFL.portable_repr import print_prepr as show

    >>> Border    = Property ("border")

    >>> show (Border (color = "red", width = "2px"))
    {'border-color' : 'red', 'border-width' : '2px'}
    >>> show (Border (color = "red", width = "2px", radius = "2px"))
    {'border-color' : 'red', 'border-radius' : '2px', 'border-width' : '2px'}

    >>> show (Transform (origin = "60% 100%", translate = "100px"))
    {'transform-origin' : '60% 100%', 'transform-translate' : '100px'}

    >>> show (Transform ("rotate(-45deg)", origin = "60% 100%", translate = "100px"))
    {'transform' : 'rotate(-45deg)', 'transform-origin' : '60% 100%', 'transform-translate' : '100px'}

    >>> show (Align_Items ("center"))
    {'align-items' : 'center'}

    >>> show (Display ("flex"))
    {'display' : 'flex'}

    >>> show (Flex  (flow = "row wrap", grow = 8))
    {'flex-flow' : 'row wrap', 'flex-grow' : 8}

    >>> show (Order (3))
    {'order' : '3'}

    >>> show (Width ("min_content"))
    {'width' : 'min-content'}

"""

from   _CHJ                       import CHJ
from   _TFL                       import TFL

from   _CHJ.Parameters            import P_dict
import _CHJ._CSS

from   _TFL._Meta.Once_Property   import Once_Property
from   _TFL.pyk                   import pyk

import _TFL._Meta.Object

from   itertools                  import chain as ichain

class _Prop_ (TFL.Meta.Object) :

    def __init__ (self, _name) :
        self.name      = _name
    # end def __init__

# end class _Prop_

class Property (_Prop_) :
    """CSS property specification."""

    def __call__ (self, * args, ** decls) :
        """Return a dict with `decls`."""
        result    = {}
        name      = self.name
        as_text   = str
        if args :
            v = result [name] = " ".join (as_text (a) for a in args)
        for k, v in pyk.iteritems (decls) :
            k = k.replace ("_", "-")
            n = "-".join ((name, k))
            result [n] = v
        return result
    # end def __call__

    @Once_Property
    def P (self) :
        """Property parameter dict: supports lazy evaluation of dict arguments.
        """
        cls = self.__class__
        def _P__call__ (this, P) :
            return self (** P_dict.__call__ (this, P))
        return P_dict.__class__ \
            ( "P_%s" % (cls.__name__)
            , (P_dict, )
            , dict
                ( __call__   = _P__call__
                , __module__ = cls.__module__
                )
            )
    # end def P

# end class Property

class Value (_Prop_) :
    """CSS value specification."""

    def __call__ (self, value) :
        name   = self.name.replace   ("_", "-")
        v      = str (value).replace ("_", "-")
        result = {name : v}
        return result
    # end def __call__

# end class Value

Border              = Property ("border")
Box                 = Property ("box")
Column              = Property ("column")
Filter              = Property ("filter")
Flex                = Property ("flex")
Flex_Direction      = Property ("flex-direction")
Font_Feat           = Property ("font-feature-settings")
Gradient            = Property ("gradient")
Transform           = Property ("transform")
Transition          = Property ("transition")

Align_Content       = Value    ("align_content")
Align_Items         = Value    ("align_items")
Align_Self          = Value    ("align_self")
Display             = Value    ("display")
Justify_Content     = Value    ("justify_content")
Order               = Value    ( "order")
Scroll_Snap_Align   = Value    ("scroll-snap-align")
Scroll_Snap_Type    = Value    ("scroll-snap-type")
Width               = Value    ("width")

if __name__ != "__main__" :
    CHJ.CSS._Export ("*")
### __END__ CHJ.CSS.Property
