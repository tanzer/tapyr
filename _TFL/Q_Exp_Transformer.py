# -*- coding: utf-8 -*-
# Copyright (C) 2024 Christian Tanzer All rights reserved
# tanzer@gg32.com.
# #*** <License> ************************************************************#
# This module is part of the package TFL.
#
# This module is licensed under the terms of the BSD 3-Clause License
# <http://www.gg32.com/license/bsd_3c.html>.
# #*** </License> ***********************************************************#
#
#++
# Name
#    TFL.Q_Exp_Transformer
#
# Purpose
#    Transform a query expression defined by `TFL.Q_Exp.Q`
#
# Revision Dates
#     4-Nov-2024 (CT) Creation
#     7-Nov-2024 (CT) Add support for `_AX` and `_MX`
#    ««revision-date»»···
#--

from   _TFL                       import TFL

import _TFL._Meta.Object
import _TFL.Accessor
import _TFL.Decorator
import _TFL.Filter
import _TFL.Undef

from   _TFL.Math_Func             import average
from   _TFL.predicate             import callable
from   _TFL.portable_repr         import portable_repr
from   _TFL.pyk                   import pyk
from   _TFL.Q_Exp                 import Q
from   _TFL._Meta.Single_Dispatch import Single_Dispatch, Single_Dispatch_Method

from   types                      import MethodType as method_type

import operator

class QXT (TFL.Meta.Object) :
    """Provide generic structure for transformations of query expressions."""

    class Context (TFL.Meta.Object) :
        """Context passed to transformation methods."""

        def __init__ (self, ** kwds) :
            self._kwds = kwds
            self.__dict__.update (kwds)
        # end def __init__

        def inner (self) :
            return self
        # end def inner

    # end class Context

    def __call__ (self, qx, context = None) :
        """Return transformed query expression `qx`"""
        if context is None :
            context = self.Context ()
        return self.qxt (qx, context)
    # end def __call__

    @Single_Dispatch_Method
    def qxt (self, qx, context) :
        """Return transformed query expression `qx`"""
        return self._qxt (qx, context.inner ())
    # end def qxt

    @qxt.add_type (Q._Aggr_)
    def __qxt_aggr (self, qx, context) :
        return self._qxt_aggr (qx, context.inner ())
    # end def __qxt_aggr

    @qxt.add_type (Q._AND_)
    def __qxt_and (self, qx, context) :
        return self._qxt_and (qx, context.inner ())
    # end def __qxt_and

    @qxt.add_type (Q._AX_)
    def __qxt_ax (self, qx, context) :
        return self._qxt_ax (qx, context.inner ())
    # end def __qxt_ax

    @qxt.add_type (Q._MX_Clos_)
    def __qxt_mx_clos (self, qx, context) :
        return self._qxt_mx_clos (qx, context.inner ())
    # end def __qxt_mx_clos

    @qxt.add_type (Q._MX_Open_)
    def __qxt_mx_open (self, qx, context) :
        return self._qxt_mx_open (qx, context.inner ())
    # end def __qxt_mx_open

    @qxt.add_type (Q.APPLY_LATE)
    def __qxt_apply_late (self, qx, context) :
        return self._qxt_apply_late (qx, context.inner ())
    # end def __qxt_apply_late

    @qxt.add_type (Q._Bin_)
    def __qxt_bin (self, qx, context) :
        return self._qxt_bin (qx, context.inner ())
    # end def __qxt_bin

    @qxt.add_type (Q.BVAR.BVAR)
    def __qxt_bvar (self, qx, context) :
        return self._qxt_bvar (qx, context.inner ())
    # end def __qxt_bvar

    @qxt.add_type (Q._Call_)
    def __qxt_call (self, qx, context) :
        return self._qxt_call (qx, context.inner ())
    # end def __qxt_call

    @qxt.add_type (Q._Get_)
    def __qxt_get (self, qx, context) :
        return self._qxt_get (qx, context.inner ())
    # end def __qxt_get

    @qxt.add_type (method_type)
    def __qxt_method (self, qx, context) :
        return self._qxt_method (qx, context.inner ())
    # end def __qxt_method

    @qxt.add_type (Q._NIL_)
    def __qxt_nil (self, qx, context) :
        return self._qxt_nil (qx, context.inner ())
    # end def __qxt_nil

    @qxt.add_type (Q.NOT)
    def __qxt_not (self, qx, context) :
        return self._qxt_not (qx, context.inner ())
    # end def __qxt_not

    @qxt.add_type (Q._OR_)
    def __qxt_or (self, qx, context) :
        return self._qxt_or (qx, context.inner ())
    # end def __qxt_or

    @qxt.add_type (Q._Self_)
    def __qxt_self (self, qx, context) :
        return self._qxt_self (qx, context.inner ())
    # end def __qxt_self

    @qxt.add_type (Q.TUPLE)
    def __qxt_tuple (self, qx, context) :
        return self._qxt_tuple (qx, context.inner ())
    # end def __qxt_tuple

    @qxt.add_type (Q._Una_)
    def __qxt_una (self, qx, context) :
        return self._qxt_una (qx, context.inner ())
    # end def __qxt_una

    @qxt.add_type (TFL.Filter_And)
    def __qxt_filter_and (self, qx, context) :
        return self._qxt_filter_and (qx, context.inner ())
    # end def __qxt_filter_and

    @qxt.add_type (TFL.Filter_Or)
    def __qxt_filter_or (self, qx, context) :
        return self._qxt_filter_or (qx, context.inner ())
    # end def __qxt_filter_or

    ### Redefine any of the following functions to transform as you want
    def _qxt (self, qx, context) :
        return qx
    # end def _qxt

    def _qxt_aggr (self, qx, context) :
        rhs = self.qxt (qx.rhs, context)
        return qx.__class__ (rhs)
    # end def _qxt_aggr

    def _qxt_and (self, qx, context) :
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
        return qx.Q.AND (* ps)
    # end def _qxt_and

    def _qxt_ax (self, qx, context) :
        return qx
    # end def _qxt_ax

    def _qxt_mx_clos (self, qx, context) :
        return qx
    # end def _qxt_mx_clos

    def _qxt_mx_open (self, qx, context) :
        return qx
    # end def _qxt_mx_open

    def _qxt_apply_late (self, qx, context) :
        return qx
    # end def _qxt_apply_late

    def _qxt_bin (self, qx, context) :
        lhs = self.qxt (qx.lhs, context)
        rhs = self.qxt (qx.rhs, context)
        return qx.__class__ (lhs, qx.op, rhs, qx.undefs, qx.reverse)
    # end def _qxt_bin

    def _qxt_bvar (self, qx, context) :
        return qx
    # end def _qxt_bvar

    def _qxt_call (self, qx, context) :
        lhs     = self.qxt (qx.lhs, context)
        args    = tuple (self.qxt (a, context) for a in qx.args)
        return qx.Q._Call_ (lhs, qx.op, * args, ** qx.kw)
    # end def _qxt_call

    def _qxt_get (self, qx, context) :
        return qx
    # end def _qxt_get

    def _qxt_method (self, qx, context) :
        return qx
    # end def _qxt_method

    def _qxt_nil (self, qx, context) :
        return qx
    # end def _qxt_nil

    def _qxt_not (self, qx, context) :
        lhs = self.qxt  (qx.lhs, context)
        return qx.Q.NOT (lhs)
    # end def _qxt_not

    def _qxt_or (self, qx, context) :
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
        return qx.Q.OR (* ps)
    # end def _qxt_or

    def _qxt_self (self, qx, context) :
        return qx
    # end def _qxt_self

    def _qxt_tuple (self, qx, context) :
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
        return qx.Q.TUPLE (* ps)
    # end def _qxt_tuple

    def _qxt_una (self, qx, context) :
        lhs = self.qxt (qx.lhs, context)
        return qx.__class__ (lhs, qx.op, qx.undefs)
    # end def _qxt_una

    def _qxt_filter_and (self, qx, context) :
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
        return TFL.Filter_And (* ps)
    # end def _qxt_filter_and

    def _qxt_filter_or (self, qx, context) :
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
        return TFL.Filter_Or (* ps)
    # end def _qxt_filter_or

# end class QXT

class QXT_Tree (QXT) :
    """Transform query expression into tree display.

    This an example of how to use `QXT` to transform a query expression and a
    test at the same time.

    >>> qxtt = QXT_Tree ()

    >>> print (qxtt (Q.foo))
    Getter for: foo

    >>> print (qxtt (Q.foo.bar))
    Getter for: foo.bar

    >>> print (qxtt (Q.foo * Q.bar / 100))
    Binary operator: /
      Binary operator: *
        Getter for: foo
        Getter for: bar
      Constant: 100

    >>> print (qxtt (Q.foo & Q.bar))
    Boolean: AND
      Getter for: foo
      Getter for: bar

    >>> print (qxtt (Q.AND (Q.foo, Q.bar)))
    Boolean: AND
      Getter for: foo
      Getter for: bar

    >>> print (qxtt (Q.AND (Q.foo, Q.bar) == 0))
    Filter_And
      Binary operator: ==
        Getter for: foo
        Constant: 0
      Binary operator: ==
        Getter for: bar
        Constant: 0

    >>> print (qxtt (Q.SUM (Q.TUPLE (Q.foo, Q.bar, Q.qux.le))))
    Aggregator: SUM
      TUPLE
        Getter for: foo
        Getter for: bar
        Getter for: qux.le

    >>> print (qxtt (Q.SUM))
    Method: SUM

    >>> print (qxtt (Q.foo.BETWEEN))
    Method: BETWEEN

    >>> print (qxtt (Q.foo.BETWEEN (1, 12)))
    Function: between
      Getter for: foo
      Constant: 1
      Constant: 12

    >>> print (qxtt (Q.FCT (Q.isoformat)))
    Function: call
      Getter for: isoformat

    >>> print (qxtt (Q.APPLY_LATE (Q.hour, Q.SELF - Q.FCT (Q.dst))))
    APPLY LATE (apply 1. expression to result of 2. expression)
      Getter for: hour
      Binary operator: -
        Q.SELF
        Function: call
          Getter for: dst

    >>> print (qxtt ((Q.SELF - Q.FCT (Q.dst))._AX.hour))
    AX (recursive attribute query)
      Binary operator: -
        Q.SELF
        Function: call
          Getter for: dst
      Getter for: hour

    >>> print (qxtt (Q.dt._MX.isoformat ()))
    MX closure (recursive method query)
      Getter for: dt
      Method: isoformat

    >>> print (qxtt (Q.dt._MX.strftime ("%Y-%m-%d %a")))
    MX closure (recursive method query)
      Getter for: dt
      Method: strftime
        Arguments
          Constant: %Y-%m-%d %a

    >>> print (qxtt ((Q.SELF - Q.FCT (Q.dst))._AX.hour))
    AX (recursive attribute query)
      Binary operator: -
        Q.SELF
        Function: call
          Getter for: dst
      Getter for: hour

    >>> print (qxtt ((Q.SELF - Q.SELF._MX.dst ())._MX.strftime ("%Y/%m/%d")))
    MX closure (recursive method query)
      Binary operator: -
        Q.SELF
        MX closure (recursive method query)
          Q.SELF
          Method: dst
      Method: strftime
        Arguments
          Constant: %Y/%m/%d

    >>> print (qxtt ((Q.dt2 - Q.dt1)._AX.seconds / 3))
    Binary operator: /
      AX (recursive attribute query)
        Binary operator: -
          Getter for: dt2
          Getter for: dt1
        Getter for: seconds
      Constant: 3

    """

    class Tree_Context (QXT.Context) :

        def __init__ (self, level = -1, ** kwds) :
            kwds.update (level = level)
            if "tree" not in kwds :
                kwds.update (tree = [])
            super ().__init__ (** kwds)
        # end def __init__

        def add (self, * args) :
            indent  = "  " * self.level
            self.tree.extend ("%s%s" % (indent, arg) for arg in args)
        # end def add

        def inner (self) :
            return self.__class__ \
                (** dict (self._kwds, level = self.level + 1))
        # end def inner

    Context = Tree_Context # end class

    def __call__ (self, qx, context = None) :
        if context is None :
            context = self.Context ()
        self.qxt (qx, context)
        return "\n".join (context.tree)
    # end def __call__

    def _qxt (self, qx, context) :
        context.add ("Constant: %s" % qx)
    # end def _qxt

    def _qxt_aggr (self, qx, context) :
        context.add ("Aggregator: %s" % qx.op_name)
        rhs = self.qxt (qx.rhs, context)
    # end def _qxt_aggr

    def _qxt_and (self, qx, context) :
        context.add ("Boolean: AND")
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
    # end def _qxt_and

    def _qxt_ax (self, qx, context) :
        context.add ("AX (recursive attribute query)")
        lhs  = self.qxt (qx.lhs, context)
        icon = context.inner ()
        icon.add ("Getter for: %s" % qx._name)
    # end def _qxt_ax

    def _qxt_mx_clos (self, qx, context) :
        context.add ("MX closure (recursive method query)")
        lhs  = self.qxt (qx.lhs, context)
        icon = context.inner ()
        icon.add ("Method: %s" % qx.m_name)
        if qx.args :
            icon.inner ().add ("Arguments")
            icon = icon.inner ()
            for a in qx.args :
                self.qxt (a, icon)
    # end def _qxt_mx_clos

    def _qxt_mx_open (self, qx, context) :
        context.add ("MX open (recursive method query)")
        lhs  = self.qxt (qx.lhs, context)
        icon = context.inner ()
        icon.add ("Method: %s" % qx._name)
    # end def _qxt_mx_open

    def _qxt_apply_late (self, qx, context) :
        context.add \
            ("APPLY LATE (apply 1. expression to result of 2. expression)")
        self.qxt (qx.qo, context)
        self.qxt (qx.qi, context)
    # end def _qxt_apply_late

    def _qxt_bin (self, qx, context) :
        op = qx.op.__name__
        context.add ("Binary operator: %s" % qx.op_map.get (op, op))
        lhs = self.qxt (qx.lhs, context)
        rhs = self.qxt (qx.rhs, context)
    # end def _qxt_bin

    def _qxt_bvar (self, qx, context) :
        context.add ("Bounded variable: %s" % qx._name)
    # end def _qxt_bvar

    def _qxt_call (self, qx, context) :
        context.add ("Function: %s" % qx.op.__name__)
        lhs     = self.qxt (qx.lhs, context)
        icon    = context.inner ()
        args    = tuple (self.qxt (a, icon) for a in qx.args)
    # end def _qxt_call

    def _qxt_get (self, qx, context) :
        context.add ("Getter for: %s" % qx._name)
    # end def _qxt_get

    def _qxt_method (self, qx, context) :
        context.add ("Method: %s" % qx.__name__)
    # end def _qxt_method

    def _qxt_nil (self, qx, context) :
        context.add (qx)
    # end def _qxt_nil

    def _qxt_not (self, qx, context) :
        context.add ("Boolean: NOT")
        lhs = self.qxt  (qx.lhs, context)
    # end def _qxt_not

    def _qxt_or (self, qx, context) :
        context.add ("Boolean: OR")
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
    # end def _qxt_or

    def _qxt_self (self, qx, context) :
        context.add (qx)
    # end def _qxt_self

    def _qxt_tuple (self, qx, context) :
        context.add ("TUPLE")
        ps  = tuple (self.qxt (p, context) for p in qx.qs)
    # end def _qxt_tuple

    def _qxt_una (self, qx, context) :
        op = qx.op.__name__
        context.add ("Unary operator: %s" % qx.op_map.get (op, op))
        lhs = self.qxt (qx.lhs, context)
    # end def _qxt_una

    def _qxt_filter_and (self, qx, context) :
        context.add ("Filter_And")
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
    # end def _qxt_filter_and

    def _qxt_filter_or (self, qx, context) :
        context.add ("Filter_Or")
        ps  = tuple (self.qxt (p, context) for p in qx.predicates)
    # end def _qxt_filter_or

# end class QXT_Tree

if __name__ != "__main__" :
    TFL._Export ("QXT", "QXT_Tree")
### __END__ TFL.Q_Exp_Transformer
