# -*- coding: utf-8 -*-
# Copyright (C) 2024 Christian Tanzer All rights reserved
# tanzer@gg32.com.
# #*** <License> ************************************************************#
# This module is part of the package TFL.
#
# This module is licensed under the terms of the BSD 3-Clause License
# <http://www.gg32.com/license/bsd_3c.html>.
# #*** </License> ***********************************************************#
#
#++
# Name
#    TFL.SVG
#
# Purpose
#    Provide a class to parse, query, and manipulate SVG documents
#
# Revision Dates
#    14-May-2024 (CT) Creation
#    15-May-2024 (CT) Add doctest
#    ««revision-date»»···
#--

from   _TFL                     import TFL

from   _TFL.predicate           import uniq
from   _TFL._Meta.Once_Property import Once_Property

import _TFL.CAO
import _TFL._Meta.Object

from   collections              import defaultdict
from   copy                     import copy

class SVG (TFL.Meta.Object) :
    """Parse, query, and manipulate SVG a document using bs4.BeautifulSoup."""

    in_svg                  = None

    def __init__ (self, input, parser = "xml", want_document = False) :
        if in_svg  := getattr (input, "svg", None) :
            self.in_svg     = in_svg
            input           = "\n".join (in_svg.as_xml ())
        self.input          = input
        self.parser         = parser
        self.want_document  = want_document
    # end def __init__

    @classmethod
    def from_file (cls, file_name, * args, ** kwds) :
        with open (file_name, "r") as f :
            return cls (f.read (), * args, ** kwds)
    # end def from_file

    @Once_Property
    def bs4 (self) :
        ### https://beautiful-soup-4.readthedocs.io/en/latest/
        import bs4
        return bs4
    # end def bs

    @Once_Property
    def soup (self) :
        return self.bs4.BeautifulSoup (self.input, self.parser)
    # end def soup

    def class_of (self, elem, default = None) :
        return elem.attrs.get ("class", default)
    # end def class_of

    def consolidate_defs (self, selector = "svg[version]") :
        """Consolidate `<defs>` between the elements matching `selector`.

        Multiple `<defs class="X">` are consolidated into a single top-level
        `<defs class="X">` for any class `X` except class `Style`:

        * Redundant definitions are dropped.

        * Use of one `id` for multiple different elements is flagged by
          raising an exception.

        <defs class="Style"> are currently left unchanged in each of the
        elements matching `selector`.
        """
        top, * nesteds  = self.soup.select (selector)
        defs_map        = self._defs_map   (top, * nesteds)
        ids_map         = defaultdict      (list)
        next_i          = self._after_title_index (top)
        for c, ds in defs_map.items () :
            d0  = ds [0]
            if not d0.parent :
                top.insert (next_i, d0)
                next_i += 1
            if len (ds) > 1 :
                self._merge_defs (* ds)
            if d0.children :
                self._update_ids_map (ids_map, d0.children)
        conflicts   = list \
            ((elid, els) for (elid, els) in ids_map.items () if len (els) > 1)
        if conflicts :
            raise ValueError \
                ( "Id conflicts between <defs> elements:\n  %s"
                % ( "\n  ".join
                      ( "%s ::\n    %s"
                          % (elid, "\n    ".join (str (e) for e in els))
                      for (elid, els) in conflicts
                      )
                  )
                )
    # end def consolidate_defs

    def head_of (self, elem, max_length = 200) :
        s = str (elem)
        return s [:min (s.index (">") + 1, max_length)]
    # end def head_of

    def prettified (self) :
        soup = self.soup
        elem = (soup if self.want_document else soup.svg)
        return elem.prettify ().rstrip ()
    # end def prettified

    def _after_title_index (self, top) :
        title   = top.find ("title", recursive=False)
        if title :
            result  = top.index (title) + 1
        else :
            NS      = self.bs4.NavigableString
            for c in top.children :
                if not isinstance (c, NS) :
                    result  = top.index (c)
                    break
        return result
    # end def _after_title_index

    def _classy_defs (self, elem) :
        for d in elem.find_all ("defs", recursive = False) :
            c = self.class_of (d)
            if c != "Style" :
                yield c, d
    # end def _classy_defs

    def _defs_map (self, top, * nesteds) :
        map  = defaultdict (list)
        for c, d in self._classy_defs (top) :
            map [c].append (d)
        for svg in nesteds :
            for c, d in self._classy_defs (svg) :
                map [c].append (copy (d))
                d.decompose ()
        result = defaultdict (list)
        for c, ds in map.items () :
            result [c]  = list (uniq (ds))
        return result
    # end def _defs_map

    def _merge_defs (self, h_defs, * t_defss) :
        seen = set (h_defs.children)
        for t_defs in t_defss :
            for c in t_defs.children :
                if c not in seen :
                    seen.add (c)
                    h_defs.append (c)
    # end def _merge_defs

    def _update_ids_map (self, ids_map, elems) :
        for e in elems :
            try :
                elid    = e.attrs.get ("id")
            except AttributeError :
                pass
            else :
                if elid :
                    ids_map [elid].append (e)
    # end def _update_ids_map

    def __str__ (self) :
        soup = self.soup
        return str (soup if self.want_document else soup.svg)
    # end def __str__

# end class SVG

_test_defs_d_t_1 = """\
  <defs class="data">
    <text class="X">xxxx</text>
  </defs>
"""

_test_defs_d_i_1 = """\
  <defs class="data">
    <text class="Z">z*5</text>
  </defs>
"""

_test_defs_m_i_1 = """\
  <defs class="Marker">
    <marker id="mrk-1" cx="1.0"></marker>
    <marker id="mrk-2" cy="1.0"></marker>
  </defs>
"""

_test_defs_m_i_2 = """\
  <defs class="Marker">
    <marker id="mrk-1" cx="1.0"></marker>
    <marker id="mrk-3" cx="2.0"></marker>
  </defs>
"""

_test_defs_m_i_2_conflict = """\
  <defs class="Marker">
    <marker id="mrk-1" cy="1.0"></marker>
    <marker id="mrk-3" cy="2.0"></marker>
  </defs>
"""

_test_defs_m_i_3 = """\
    <defs class="Marker">
      <marker id="mrk-4" cx="-1.0"></marker>
      <marker id="mrk-5" cx="-2.0"></marker>
    </defs>
"""

_test_defs_s_i_1 = """\
    <defs class="Symbol">
      <g id="sym-a"><line x1="0" x2="1"/></g>
    </defs>
"""

_test_defs_y_1 = """\
    <defs class="Style">
      <style type="text/css">
        text  { stroke-width: 0 }
      </style>
    </defs>
"""

test_svg_1 = """\
<svg version="1.1">
  <title>Test 1</title>
""" + _test_defs_d_t_1 + """
  <g class="vp">
    <svg version="1.1">
      <title>Test 1 inner svg 1</title>
""" + _test_defs_m_i_1 + """
""" + _test_defs_d_i_1 + """
""" + _test_defs_m_i_3 + """
""" + _test_defs_y_1 + """
    </svg>
    <svg version="1.1">
      <title>Test 1 inner svg 2</title>
""" + _test_defs_s_i_1 + """
""" + _test_defs_m_i_1 + """
""" + _test_defs_y_1 + """
""" + _test_defs_m_i_2 + """
    </svg>
  </g>
</svg>
"""

test_svg_c = """\
<svg version="1.1">
  <title>Test conflict</title>
  <g class="vp">
    <svg version="1.1">
      <title>Test conflict inner svg 1</title>
""" + _test_defs_m_i_1 + """
""" + _test_defs_m_i_2 + """
    </svg>
    <svg version="1.1">
      <title>Test conflict inner svg 2</title>
""" + _test_defs_m_i_1 + """
""" + _test_defs_m_i_2_conflict + """
    </svg>
  </g>
</svg>
"""

__test__ = dict \
    ( TFL_SVG = """\

    Test normal case:
    >>> svg1 = SVG (test_svg_1)
    >>> print (svg1.prettified ()) ### original
    <svg version="1.1">
     <title>
      Test 1
     </title>
     <defs class="data">
      <text class="X">
       xxxx
      </text>
     </defs>
     <g class="vp">
      <svg version="1.1">
       <title>
        Test 1 inner svg 1
       </title>
       <defs class="Marker">
        <marker cx="1.0" id="mrk-1"/>
        <marker cy="1.0" id="mrk-2"/>
       </defs>
       <defs class="data">
        <text class="Z">
         z*5
        </text>
       </defs>
       <defs class="Marker">
        <marker cx="-1.0" id="mrk-4"/>
        <marker cx="-2.0" id="mrk-5"/>
       </defs>
       <defs class="Style">
        <style type="text/css">
         text  { stroke-width: 0 }
        </style>
       </defs>
      </svg>
      <svg version="1.1">
       <title>
        Test 1 inner svg 2
       </title>
       <defs class="Symbol">
        <g id="sym-a">
         <line x1="0" x2="1"/>
        </g>
       </defs>
       <defs class="Marker">
        <marker cx="1.0" id="mrk-1"/>
        <marker cy="1.0" id="mrk-2"/>
       </defs>
       <defs class="Style">
        <style type="text/css">
         text  { stroke-width: 0 }
        </style>
       </defs>
       <defs class="Marker">
        <marker cx="1.0" id="mrk-1"/>
        <marker cx="2.0" id="mrk-3"/>
       </defs>
      </svg>
     </g>
    </svg>

    >>> svg1.consolidate_defs ()
    >>> print (svg1.prettified ()) ### consolidated
    <svg version="1.1">
     <title>
      Test 1
     </title>
     <defs class="Marker">
      <marker cx="1.0" id="mrk-1"/>
      <marker cy="1.0" id="mrk-2"/>
      <marker cx="-1.0" id="mrk-4"/>
      <marker cx="-2.0" id="mrk-5"/>
      <marker cx="2.0" id="mrk-3"/>
     </defs>
     <defs class="Symbol">
      <g id="sym-a">
       <line x1="0" x2="1"/>
      </g>
     </defs>
     <defs class="data">
      <text class="X">
       xxxx
      </text>
      <text class="Z">
       z*5
      </text>
     </defs>
     <g class="vp">
      <svg version="1.1">
       <title>
        Test 1 inner svg 1
       </title>
       <defs class="Style">
        <style type="text/css">
         text  { stroke-width: 0 }
        </style>
       </defs>
      </svg>
      <svg version="1.1">
       <title>
        Test 1 inner svg 2
       </title>
       <defs class="Style">
        <style type="text/css">
         text  { stroke-width: 0 }
        </style>
       </defs>
      </svg>
     </g>
    </svg>

    Test normal case without top-level `<title>`:
    >>> svg1_no_title = SVG (test_svg_1.replace ("<title>Test 1</title>", ""))
    >>> svg1_no_title.consolidate_defs ()
    >>> print (svg1_no_title.prettified ()) ### consolidated
    <svg version="1.1">
     <defs class="Marker">
      <marker cx="1.0" id="mrk-1"/>
      <marker cy="1.0" id="mrk-2"/>
      <marker cx="-1.0" id="mrk-4"/>
      <marker cx="-2.0" id="mrk-5"/>
      <marker cx="2.0" id="mrk-3"/>
     </defs>
     <defs class="Symbol">
      <g id="sym-a">
       <line x1="0" x2="1"/>
      </g>
     </defs>
     <defs class="data">
      <text class="X">
       xxxx
      </text>
      <text class="Z">
       z*5
      </text>
     </defs>
     <g class="vp">
      <svg version="1.1">
       <title>
        Test 1 inner svg 1
       </title>
       <defs class="Style">
        <style type="text/css">
         text  { stroke-width: 0 }
        </style>
       </defs>
      </svg>
      <svg version="1.1">
       <title>
        Test 1 inner svg 2
       </title>
       <defs class="Style">
        <style type="text/css">
         text  { stroke-width: 0 }
        </style>
       </defs>
      </svg>
     </g>
    </svg>

    >>> svgc = SVG (test_svg_c)
    >>> with expect_except (ValueError) :
    ...     svgc.consolidate_defs ()
    ValueError: Id conflicts between <defs> elements:
      mrk-1 ::
        <marker cx="1.0" id="mrk-1"/>
        <marker cy="1.0" id="mrk-1"/>
      mrk-3 ::
        <marker cx="2.0" id="mrk-3"/>
        <marker cy="2.0" id="mrk-3"/>

"""
    )

def _main (cmd) :
    """Consolidate <defs> elements in svg file."""
    svg = SVG.from_file  (cmd.svg_file)
    svg.consolidate_defs ()
    print (svg)
# end def _main

_Command = TFL.CAO.Cmd \
    ( handler       = _main
    , args          =
        ( "svg_file:P?SVG file to consolidate"
        ,
        )
    , opts          =
        ()
    , min_args      = 1
    , max_args      = 1
    )

if __name__ != "__main__" :
    TFL._Export ("SVG")
else :
    _Command ()
### __END__ TFL.SVG
