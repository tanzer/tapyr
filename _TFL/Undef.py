# -*- coding: utf-8 -*-
# Copyright (C) 2010-2024 Christian Tanzer All rights reserved
# tanzer@gg32.com                                      https://www.gg32.com
# ****************************************************************************
# This module is part of the package TFL.
#
# This module is licensed under the terms of the BSD 3-Clause License
# <https://www.gg32.com/license/bsd_3c.html>.
# ****************************************************************************
#
#++
# Name
#    TFL.Undef
#
# Purpose
#    Provide a class for defining undefined objects with nice repr
#
# Revision Dates
#     3-Sep-2010 (CT) Creation
#    28-Sep-2010 (CT) `__nonzero__` added
#    22-Feb-2013 (CT) Add doc-tests
#    21-Aug-2014 (CT) Add `is_undefined`
#    13-Apr-2015 (CT) Add `_import_cb_json_dump`
#     6-Oct-2024 (CT) Add operator methods to `Undef`
#     7-Oct-2024 (CT) Move operator methods into factored `Undef_S`
#    ««revision-date»»···
#--

from   _TFL               import TFL
from   _TFL.pyk           import pyk

class Undef (object) :
    """Undefined object with nice repr."""

    def __init__ (self, name = None) :
        self.name = name
    # end def __init__

    def __bool__ (self) :
        return False
    # end def __bool__

    def __repr__ (self) :
        names = [self.__class__.__name__]
        if self.name :
            names.append (self.name)
        return "<%s>" % "/".join (names)
    # end def __repr__

# end class Undef

class Undef_S (Undef) :
    """Undefined object with nice repr and sticky under arithmetic."""

    def __abs__ (self) :
        return self
    # end def __abs__

    def __add__ (self, rhs) :
        return self
    # end def __add__

    def __and__ (self, rhs) :
        return self
    # end def __and__

    def __mod__ (self, rhs) :
        return self
    # end def __mod__

    def __mul__ (self, rhs) :
        return self
    # end def __mul__

    def __neg__ (self) :
        return self
    # end def __neg__

    def __or__ (self, rhs) :
        return self
    # end def __or__

    def __pos__ (self) :
        return self
    # end def __pos__

    def __pow__ (self, rhs) :
        return self
    # end def __pow__

    def __radd__ (self, rhs) :
        return self
    # end def __radd__

    def __rand__ (self, rhs) :
        return self
    # end def __rand__

    def __rmod__ (self, rhs) :
        return self
    # end def __rmod__

    def __rmul__ (self, rhs) :
        return self
    # end def __rmul__

    def __ror__ (self, rhs) :
        return self
    # end def __ror__

    def __rpow__ (self, rhs) :
        return self
    # end def __rpow__

    def __rsub__ (self, rhs) :
        return self
    # end def __rsub__

    def __rtruediv__ (self, rhs) :
        return self
    # end def __rtruediv__

    def __rxor__ (self, rhs) :
        return self
    # end def __rxor__

    def __sub__ (self, rhs) :
        return self
    # end def __sub__

    def __truediv__ (self, rhs) :
        return self
    # end def __truediv__

    def __xor__ (self, rhs) :
        return self
    # end def __xor__

# end class Undef_S

def is_undefined (value) :
    """Return True, if `value` is an instance of `Undef`."""
    return isinstance (value, Undef)
# end def is_undefined

@TFL._Add_Import_Callback ("_TFL.json_dump")
def _import_cb_json_dump (module) :
    @module.default.add_type (Undef)
    def json_encode_undef (o) :
        return None
# end def _import_cb_json_dump

__doc__ = """
:class:`Undef` provides a way to define undefined objects with a nice
and deterministic `repr`.

Normally, one would define an undefined object like this::

    >>> undefined = object ()
    >>> bool (undefined)
    True
    >>> undefined # doctest:+ELLIPSIS
    <object object at ...>

This works well, as long as `undefined` doesn't appear in any context, where
it's `repr` is taken and as long as nobody applies boolean tests to it.

:class:`Undef` avoids both these problems::

    >>> undef = Undef ()
    >>> bool (undef)
    False
    >>> undef
    <Undef>

    >>> undef_foo = Undef ("foo")
    >>> bool (undef_foo)
    False
    >>> undef_foo
    <Undef/foo>

    >>> undef_bar = Undef ("bar")
    >>> bool (undef_bar)
    False
    >>> undef_bar
    <Undef/bar>

    >>> undef_foo == undef_bar
    False
    >>> undef_foo is undef_bar
    False

    >>> undef_foo == Undef ("foo")
    False
    >>> undef_foo is Undef ("foo")
    False

:class:`Undef_S` supports arithmetic operators which all return `Undef_S`
instances unchanged::

    >>> undefs_foo = Undef_S ("foo")
    >>> undefs_bar = Undef_S ("bar")
    >>> undefs_foo + undefs_bar
    <Undef_S/foo>

    >>> 3 - undefs_bar
    <Undef_S/bar>

.. moduleauthor:: Christian Tanzer <tanzer@swing.co.at>

"""

if __name__ != "__main__" :
    TFL._Export ("Undef", "Undef_S", "is_undefined")
### __END__ TFL.Undef
